public class Employee extends Person{
	public String getName(){
		System.out.println("Employee Name : "+name);
		return name;
	}
	public static void main(String[] args){
		Person ref;
		Student studentObj = new Student();
		Employee employeeObj = new Employee();
		
		ref = studentObj;

		String temp = ref.getName();
		System.out.println(temp);

		ref=employeeObj;

		temp=ref.getName();
		System.out.println(temp);
	}
}